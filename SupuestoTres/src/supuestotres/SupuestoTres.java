/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supuestotres;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/**
 * @author David Casanova
 */
public class SupuestoTres {
    // Caputar input del teclado
    public static BufferedReader teclado = new BufferedReader ( new InputStreamReader( System.in ));
    

    public static void main(String[] args) {
        // Declarar variables
        String edadStr = "", categoria = "";
        int edadInt = 0;
        boolean esInt = false;
        boolean rangoValido = false;
        
        do {
            try {
                System.out.print( "Teclea la edad: " );
                edadStr = teclado.readLine();
                edadInt = Integer.parseInt( edadStr );
                
                // Si no hay excepciones entonces el número tecleaado
                esInt = true;
                
                if( edadInt > 0 ){
                    rangoValido = true;
                } else {
                    System.out.println( "El rango de edad es incorrecto. " );
                }
            } catch ( IOException e ) {
                System.out.println( "Error al leer del teclado." );
            } catch ( NumberFormatException e) {
                System.out.println( "Debes teclear un número entero." );
            }
        } while( esInt == false || rangoValido == false );
        
        // Mostrar la categoría
        if( edadInt >= 0 && edadInt <= 25 ){
            System.out.println( "La edad " + edadInt + " está dentro de la categoría A[0-25]" );
        } else if ( edadInt >= 26 && edadInt <= 50 ) {
            System.out.println( "La edad " + edadInt + " está dentro de la categoría B[26-50]" );
        } else if ( edadInt >= 51 && edadInt <= 75 ) {
            System.out.println( "La edad " + edadInt + " está dentro de la categoría C[51-75]" );
        } else if ( edadInt >= 76 ) {
            System.out.println( "La edad " + edadInt + " está dentro de la categoría D[76-...]" );
        }
    }
    
}
