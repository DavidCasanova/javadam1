/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mayusculasminusculas;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 *
 * @author David Casanova
 */
public class MayusculasMinusculas {
    // Capturar entrada por teclado
    public static BufferedReader input = new BufferedReader ( new InputStreamReader( System.in ));
    
    public static void main(String[] args) {
        // Variables
        String line;
        
        // Expresión regular para mayúsculas y minúsculas
        String regexLowercase = "[a-z]";
        String regexUppercase = "[A-Z]";
        
        try {
            System.out.print( "Tecla un único caracter: ");
            line = input.readLine();
            
            if( line.length() > 1 ){
                System.out.println( "Has tecleado más de un carcater." );
            } else {
                if( Pattern.matches( regexUppercase, line) == true ){
                    System.out.println( "El caracter \"" + line + "\" es una mayúscula." );
                } else if ( Pattern.matches( regexLowercase, line ) == true  ){
                    System.out.println( "El caracter \"" + line + "\" es una minúscula." );
                }
            }
        } catch ( IOException e ) {
            System.out.println( "Error al leer del teclado." );
        }
    }
    
}
