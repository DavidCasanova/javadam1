package aplicacioncuentabancaria;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/**
 *
 * @author David Casanova
 */
public class AplicacionCuentaBancaria {
    // Capturar entrada desde el teclado
    public static BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Antes de mostrar el menú procedemos a crear la cuenta
        boolean titularValido = false, entidadValida = false, oficinaValida = false, cuentaValida = false, salir = false;
        String titular = "", entidad = "", oficina = "", cuenta = "";
        int opcion = 0;
        
        // Titular
        do{
            try{
                System.out.println("Teclea el titular de la cuenta (el el nombre debe tener entre " + CuentaBancaria.MIN_LON_TITULAR + " y " + CuentaBancaria.MAX_LON_TITULAR + "):");
                titular = teclado.readLine();
                if(titular.length() < CuentaBancaria.MIN_LON_TITULAR) {
                    System.out.println("El nombre del titular es demasiado corto.");
                }else if(titular.length() > CuentaBancaria.MAX_LON_TITULAR){
                    System.out.println("El nombre del titular es demasiado largo.");
                }else{
                    titularValido = true;
                }
                
                
            } catch(IOException e){
                System.out.println("Error al leer del teclado.");
            }
        } while(titularValido == false);
        
        // Entidad
        do{
            try{
                System.out.println("Tecla la entidad (debe tener 4 caracteres):");
                entidad = teclado.readLine();
                if(entidad.length() != 4) {
                    System.out.println("La entidad debe tener cuatro caracteres.");
                }else{
                    entidadValida = true;
                }
            } catch(IOException e){
                System.out.println("Error al leer del teclado.");
            }
        } while(entidadValida == false);
        
        // Oficina
        do{
            try{
                System.out.println("Tecla la oficina (debe tener 4 caracteres):");
                oficina = teclado.readLine();
                if(oficina.length() != 4) {
                    System.out.println("La oficina debe tener cuatro caracteres.");
                }else{
                    oficinaValida = true;
                }
            } catch(IOException e){
                System.out.println("Error al leer del teclado.");
            }
        } while(oficinaValida == false);
        
        // Cuenta
        do{
            try{
                System.out.println("Tecla la cuenta (debe tener 10 caracteres):");
                cuenta = teclado.readLine();
                if(cuenta.length() != 10) {
                    System.out.println("La cuenta debe tener 10 caracteres.");
                }else{
                    cuentaValida = true;
                }
            } catch(IOException e){
                System.out.println("Error al leer del teclado.");
            }
        } while(cuentaValida == false);
        
        // Una vez superadas todas las comprobaciones creamos la cuenta
        CuentaBancaria cuentaBancaria = new CuentaBancaria(titular, entidad, oficina, cuenta);
        
        System.out.println("");
        System.out.println("¡Cuenta creada correctamente!");
        
        // Mostrar el menú
        while(!salir){
            System.out.println("");
            System.out.println("********************");
            System.out.println("");
            System.out.println("[1] Ver número de cuenta completo (CCC)");
            System.out.println("[2] Ver el titular de la cuenta");
            System.out.println("[3] Consular el saldo");
            System.out.println("[4] Realizar un ingreso");
            System.out.println("[5] Retirar efectivo");
            System.out.println("[6] Mostrar tipos de cuentas");
            System.out.println("[7] Salir");
            System.out.println("");
            System.out.println("********************");
            System.out.println("");
            System.out.print("Selecciona una opción: ");
            
            try{
                String opt = teclado.readLine();
                opcion = Integer.parseInt(opt);
            } catch (IOException e) {
                System.out.println("Error al leer del teclado");
            } catch (NumberFormatException e){
                System.out.println("Selecciona una opción válida");
            }
            
            switch(opcion) {
                case 1:
                    System.out.println("El número de cuenta es: " + cuentaBancaria.obtenerCCC());
                    break;
                case 2:
                    System.out.println("El titular de la cuenta es: " + cuentaBancaria.getTitular());
                    break;
                case 3:
                    System.out.println("El saldo actual es: " + cuentaBancaria.getSaldo());
                    break;
                case 4:
                    try{
                        System.out.print("Indica el importe a ingresar: ");
                        
                        String importeIngreso = teclado.readLine();
                        double ingreso = Double.parseDouble(importeIngreso);
                        
                        double resultadoIngreso = cuentaBancaria.ingresar(ingreso);
                        
                        System.out.println("El nuevo saldo de la cuenta es: " + resultadoIngreso);
                    } catch (IOException e) {
                        System.out.println("Error al leer del teclado");
                    } catch (NumberFormatException e){
                        System.out.println("El formato del importe tecleado es incorrecto");
                    } catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 5:
                    try{
                        System.out.print("Indica el importe a retirar: ");
                        
                        String importeRetirada = teclado.readLine();
                        double retirada = Double.parseDouble(importeRetirada);
                        
                        double resultadoRetirada = cuentaBancaria.retirar(retirada);
                        
                        System.out.println("El nuevo saldo de la cuenta es: " + resultadoRetirada);
                    } catch (IOException e) {
                        System.out.println("Error al leer del teclado");
                    } catch (NumberFormatException e){
                        System.out.println("El formato del importe tecleado es incorrecto");
                    } catch (Exception e){
                        System.out.println(e.getMessage());
                    }
                    break;
                case 6:
                    System.out.println(CuentaBancaria.mostrarTiposDeCuenta());
                    break;
                case 7:
                    System.out.println("¡Hasta la próxima!");
                    salir = true;
                    break;
                default:
                    System.out.println("Selecciona una opción válida");
            }
        }
    }
    
}
