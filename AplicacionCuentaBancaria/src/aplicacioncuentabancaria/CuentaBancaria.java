package aplicacioncuentabancaria;

/**
 *
 * @author David Casanova
 */
public class CuentaBancaria {
    /**
     * Atributos
     */
    private double saldo = 1118.15;
    private String titular, entidad, oficina, cuenta;
    
    public static final int MIN_LON_TITULAR = 1, MAX_LON_TITULAR = 20;
    
    /**
     * Constructores
     */
    // En blanco
    public CuentaBancaria() {
        
    }
    
    // Con parámetros
    public CuentaBancaria(String titular, String entidad, String oficina, String cuenta){
        this.titular = titular;
        this.entidad = entidad;
        this.oficina = oficina;
        this.cuenta = cuenta;
    }
    
    // Copia
    public CuentaBancaria(CuentaBancaria cuenta){
        this.titular = cuenta.getTitular();
        this.entidad = cuenta.getEntidad();
        this.oficina = cuenta.getOficina();
        this.cuenta = cuenta.getCuenta();
    }
    
    /**
     * Setter and Getters
     */
    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public double getSaldo() {
        return saldo;
    }

    public String getTitular() {
        return titular;
    }

    public String getEntidad() {
        return entidad;
    }

    public String getOficina() {
        return oficina;
    }

    public String getCuenta() {
        return cuenta;
    }

    
    /**
     * Métodos
     */
    public String obtenerCCC() {
        return this.entidad + "-" + this.oficina + "-" + this.cuenta;
    }
    
    public double ingresar(double ingreso) throws Exception {
        if(ingreso <= 0) {
            throw new Exception("Cantidad no válida");
        }else{
            this.saldo += ingreso;
            return this.saldo;
        }
    }
    
    public double retirar(double retirada) throws Exception {
        if(retirada <= 0) {
            throw new Exception("Cantidad no válida");
        }else if(retirada > this.saldo){
            throw new Exception("Saldo insuficiente");
        }else{
            this.saldo -= retirada;
            return this.saldo;
        }
    }
    
    public static String mostrarTiposDeCuenta() {
        return "Los tipos de cuenta disponibles son: Corriente, Joven y Nómina.";
    }
    
}
