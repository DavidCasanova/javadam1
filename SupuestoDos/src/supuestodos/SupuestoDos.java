/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supuestodos;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/**
 * @author David Casanova
 */
public class SupuestoDos {
    // Caputar input del teclado
    public static BufferedReader teclado = new BufferedReader ( new InputStreamReader( System.in ));
    
    public static void main(String[] args) {
        String mesStr = "", anioStr = "";
        int mesInt = 0, anioInt = 0, diasMes = 0;
        boolean esMesInt = false, esAnioInt = false, rangoMesValido = false, rangoAnioValido = false, esBisiesto = false;
        
        try {
            do {
                System.out.print( "Indica un mes del año, recuerda que debe estar entre el 1 y el 12: " );
                mesStr = teclado.readLine();
                mesInt = Integer.parseInt( mesStr );
                
                // Si no hay excepción es que el número tecleado es entero
                esMesInt = true;
                
                // Comprobar que el mes está comprendido entre el 1 y el 12
                if( mesInt >= 1 && mesInt <= 12 ) {
                    rangoMesValido = true;
                } else {
                    System.out.println( "El mes tecleado está fuera de rango." );
                }
                
            } while( esMesInt == false || rangoMesValido == false );
        } catch ( IOException e ){
            System.out.println( "Error al leer del teclado." );
        } catch ( NumberFormatException e ){
            System.out.println( "Debes teclear un número entero para el mes." );
        }
        
        try {
            do {
                System.out.print( "Indica un año, debe ser un valor entero positivo: " );
                anioStr = teclado.readLine();
                anioInt = Integer.parseInt( anioStr );
                
                // Si no hay excepción es que el número tecleado es entero
                esAnioInt = true;
                
                // Comprobar que el año es un valor positivo
                if( anioInt > 0 ) {
                    rangoAnioValido = true;
                } else {
                    System.out.println( "El año tecleado está fuera de rango." );
                }
                
            } while( esAnioInt == false || rangoAnioValido == false );
        } catch ( IOException e ){
            System.out.println( "Error al leer del teclado." );
        } catch ( NumberFormatException e ){
            System.out.println( "Debes teclear un número entero para el año." );
        }
        
       // Es año bisiesto?
       if ( ( anioInt % 400 ) == 0){
           esBisiesto = true;
       } else if ( ( ( anioInt % 4 ) == 0 ) && ( anioInt % 100 ) != 0 ){
           esBisiesto = true;
       }
        
       // Calcular los días del mes
       if ( mesInt == 2 ) {
           diasMes = ( esBisiesto == true ) ? 29 : 28;
       } else if ( mesInt == 1 || mesInt == 3 || mesInt == 5 || mesInt == 7 || mesInt == 8 || mesInt == 10 || mesInt == 12 ) {
           diasMes = 31;
       } else {
           diasMes = 30;
       }
       
       System.out.println( "El mes " + mesInt + " del año " + anioInt + " tiene " + diasMes + " dias." );
    }
    
}
