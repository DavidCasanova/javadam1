/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apppersona;

// Importar la clase necesaria para capturar los inputs desde el teclado
import java.util.Scanner;

/**
 *
 * @author david
 */
public class AppPersona {
    
    // Instanciar la clase Scanner para capturar inputs desde el teclado
    private static Scanner scanner = new Scanner ( System.in );
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Ejercicio 2
        AppPersona.cambiarNombrePersona();
        
        System.out.println("\n");
        
        // Ejercicio 4
        AppPersona.crearPersona();
        
        System.out.println("\n");
        
        // Ejercicio 5
        AppPersona.crearPersonaParametros();
    }
    
    // Métodos
    // Cambiar asignar un valor al atributo nombre de la clase persona (Ejercicio 2)
    public static void cambiarNombrePersona() {  
        
        // Obtener un nombre a para insertar 
    	System.out.print( "Teclee el nombre que desee: " );
    	String nombre = scanner.nextLine();
        
        // Comprobar que el nombre no está vacío
        if( "".equals(nombre)) {
            
            System.out.println( "El nombre no puede estar vacío" );
            
        } else { 
            
            Persona cambiar = new Persona();
            
            // Si no lo está, asignamos su valor atributo nombre de la clase persona
            cambiar.cambiaNombre(nombre);
            
            // Mostrar el valor del atributo nombre
            System.out.println( "El nombre tecleado es: " + cambiar.consultaNombre() );
            
        }
        
    }
    
    // Instanciar al constructor de la clase Persona y mostrar por pantalla el valor de los atributos (Ejercicio 4)
    public static void crearPersona() {

        System.out.println( "Vamos a crear una persona." );
        
        // Instanciar a la clase persona
        Persona crear = new Persona();
        
        // Mostrar los atributos por pantalla
        System.out.println( "Persona creada con los siguientes atributos." );
        System.out.println( "El nombre es: " + crear.consultaNombre() );
        System.out.println( "La edad es: " + crear.consultaEdad() );
        System.out.println( "La altura es: " + crear.consultaAltura() );
        
    }
    
    /* Instanciar al constructor de la clase Persona pasando parçametros y mostrar por pantalla 
    el valor de los atributos (Ejercicio 5) */
    private static void crearPersonaParametros() {
        
        System.out.println( "Vamos a crear la siguiente persona: David, 37 años y 1.87 metros de altura." );
        
        // Instanciar a la clase persona con los parámetros elegidos
        Persona crear = new Persona("David", 37, 1.87F);
        
        // Mostrar los atributos por pantalla
        System.out.println( "Persona creada con los siguientes atributos." );
        System.out.println( "El nombre es: " + crear.consultaNombre() );
        System.out.println( "La edad es: " + crear.consultaEdad() );
        System.out.println( "La altura es: " + crear.consultaAltura() );
        
    }
    
}
