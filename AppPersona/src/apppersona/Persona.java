/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apppersona;

/**
 *
 * @author david
 */
public class Persona {
       
    // Atributos
    private String nombre;
    private int edad;
    private float altura;
    
    // Constructor
    public Persona() {
        this.nombre = "";
        this.edad = 0;
        this.altura = 0.0F;
    }
    
    public Persona(String nombre, int edad, float altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.altura = altura;
    }
    
    // Métodos
    // Para nombre
    public String consultaNombre() {
        return nombre;
    }

    public void cambiaNombre( String nombre ) {
        this.nombre = nombre;
    }
    
    // Para edad
    public int consultaEdad() {
        return edad;
    }
    
    public void cambiaEdad( int edad ) {
        this.edad = edad;
    }
    
    // Para altura
    public float consultaAltura() {
        return altura;
    }
    
    public void cambiaAltura( float altura ) {
        this.altura = altura;
    }
}
