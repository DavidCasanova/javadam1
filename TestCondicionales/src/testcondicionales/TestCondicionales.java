package testcondicionales;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author David Casanova
 */
public class TestCondicionales {
    // Leer del teclado
    private static BufferedReader input = new BufferedReader( new InputStreamReader ( System.in ) );
    
    public static void main( String[] args ){
        // Declaración de variables
        String line;
        int numero;
        double division;
        boolean isInteger = false;
        
        do {
            try {
                System.out.print( "Teclea un número entero: " );
                line = input.readLine();
                numero = Integer.parseInt( line );
                isInteger = true;
                
                division = numero / 2;
                
                if( division % 2 != 0 ) {
                    System.out.println( "El número " + numero + " es el doble de un número impar ( " + division + ")" );
                } else {
                    System.out.println( "El número " + numero + " no es el doble de un número impar." );
                }
      
            } catch ( IOException e ) {
                System.out.println( "Error al leer del teclado." );
            } catch( NumberFormatException e ) {
                System.out.println( "¡Cuidado!, sólo se puede teclear un número entero." );
            }
            
        } while ( isInteger == false );
    }
    
}
