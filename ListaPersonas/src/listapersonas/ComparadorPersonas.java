package listapersonas;

import java.util.Comparator;


/**
 * @author David Casanova
 */
public class ComparadorPersonas implements Comparator<Persona> {
    @Override
    public int compare(Persona p1, Persona p2) {
        if ( p1.getNombre().compareTo( p2.getNombre() ) < 0 )
            return -1;
        else if ( p1.getNombre().compareTo( p2.getNombre() ) > 0 )
            return 1;
        else
            return 0;
    }
}
