package listapersonas;

/**
 *
 * @author David Casanova
 */
public class Persona {
    /**
     * Atrubutos
     */
    String nombre;
    int edad;
    
    /**
     * Contructor
     */
    public Persona( String nombre, int edad){
        this.nombre = nombre;
        this.edad = edad;
    }
    
    /**
     * Getters y setters
     */
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
    
}
