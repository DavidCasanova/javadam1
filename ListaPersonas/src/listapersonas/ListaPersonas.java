package listapersonas;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 *
 * @author david
 */
public class ListaPersonas {
    // Capturar entrada desde el teclado
    public static BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean salir = false, nombreValido, edadValida, posicionValida;
        int opcion = 0, edad = 0, posicion = 0, i = 0;
        String nombre = "", nombreEliminar = "", edadString, posicionString;
        
        // Array donde vamos a almacenar los elementos de la clase persona
        ArrayList<Persona> personas = new ArrayList<>();
        
        // Mostrar el menú
        while(!salir){
            System.out.println("");
            System.out.println("********************");
            System.out.println("");
            System.out.println("[1] Insertar una persona");
            System.out.println("[2] Borrar una persona indicando su posición");
            System.out.println("[3] Borrar una persona indicando su nombre");
            System.out.println("[4] Mostrar todas las persona (ordenadas por nombre)");
            System.out.println("[5] Modificar una persona (por posición)");
            System.out.println("[6] Borrar toda la lista");
            System.out.println("[7] Salir");
            System.out.println("");
            System.out.println("********************");
            System.out.println("");
            System.out.print("Selecciona una opción: ");
            
            try{
                String opt = teclado.readLine();
                opcion = Integer.parseInt(opt);
            } catch (IOException e) {
                System.out.println("Error al leer del teclado");
            } catch (NumberFormatException e){
                System.out.println("Selecciona una opción válida");
            }
            
            switch(opcion) {
                case 1:
                    // OPCIÓN 1: Añadir una persona
                    // Solicitar el nombre
                    nombreValido = false;
                    do{
                        try{
                            System.out.println("Teclea el nombre de la persona a crear: ");
                            nombre = teclado.readLine();
                            if(nombre.length() == 0) {
                                System.out.println("El nombre debe estar relleno");
                            }else{
                                nombreValido = true;
                            }
                        } catch(IOException e){
                            System.out.println("Error al leer del teclado.");
                        }
                    } while(nombreValido == false);
                    
                    // Solicitar la edad
                    edadValida = false;
                    do{
                        try{
                            System.out.println("Teclea la edad de " + nombre + ": ");
                            edadString = teclado.readLine();
                            edad = Integer.parseInt(edadString);
                            if(edad > 0){
                                edadValida = true;
                            }else{
                                System.out.println("La edad debe ser mayor que cero.");
                            }
                        } catch (IOException e) {
                            System.out.println("Error al leer del teclado.");
                        } catch (NumberFormatException e){
                            System.out.println("La edad debe ser un número entero.");
                        }
                    } while(edadValida == false);
                    
                    // Crear el objeto Persona
                    personas.add(new Persona( nombre, edad ));
                    break;
                case 2:
                    // OPCIÓN 2: Eliminar una persona por posición
                    // Primero comprobamos si la lista no esta vacía;
                    if(personas.isEmpty()){
                        System.out.println("No se puede eliminar elementos. La lista está vacía.");
                        break;
                    }
                    
                    // Solicitar la posición a eliminar si la lista tiene elementos
                    posicionValida = false;
                    do{
                        try{
                            System.out.println("Indica la posición a eliminar:");
                            posicionString = teclado.readLine();
                            posicion = Integer.parseInt(posicionString);
                            if(posicion < 0){
                                System.out.println("La posición debe ser un número positivo.");
                            }else if(posicion > (personas.size() - 1)){
                                System.out.println("La posición está fuera del rango, debe estar comprendida entre 0 y " + (personas.size() - 1));                               
                            }else{
                                posicionValida = true;
                            }
                        } catch (IOException e) {
                            System.out.println("Error al leer del teclado.");
                        } catch (NumberFormatException e){
                            System.out.println("La posición debe ser un número entero.");
                        }
                    } while(posicionValida == false);
                    
                    // Si todas las comprobaciones son correctas eliminamos el elemento
                    personas.remove(posicion);
                    System.out.println("Elemento eliminado correctamente.");
                    break;
                case 3:
                    // OPCIÓN 3: Eliminar una persona por nombre
                    // Primero comprobamos si la lista no esta vacía;
                    if(personas.isEmpty()){
                        System.out.println("No se puede eliminar elementos. La lista está vacía.");
                        break;
                    }
                    
                    // Solicitar el nombre del/los elemento/s a eliminar
                    nombreValido = false;
                    do{
                        try{
                            System.out.println("Teclea el nombre de la persona a eliminar: ");
                            nombreEliminar = teclado.readLine();
                            if(nombreEliminar.length() == 0) {
                                System.out.println("El nombre debe estar relleno");
                            }else{
                                nombreValido = true;
                            }
                        } catch(IOException e){
                            System.out.println("Error al leer del teclado.");
                        }
                    } while(nombreValido == false);
                    
                    // Si todas las comprobaciones son correctas eliminamos el elemento
                    Iterator<Persona> iterador = personas.iterator();
                    while(iterador.hasNext()){
                        Persona item = iterador.next();
                        if(item.getNombre().equals(nombreEliminar))
                        	// Eliminamos el Elemento que hemos obtenido del Iterator, esto lo elimina tanto del iterador como del ArrayList
                            iterador.remove();
                    }
                    System.out.println("Elemento/s eliminado/s correctamente.");
                    break;
                case 4:
                	// OPCIÓN 4: Mostrar los elementos ordenados por nombre
                    System.out.println("Los elementos de la lista son (ordenados por nombre):");
                    System.out.printf("%-3s %-20.20s  %-20.20s%n", "Pos", "Nombre", "Edad");
                    Collections.sort(personas, new ComparadorPersonas());
                    i = 0;
                    for(Persona p: personas){
                        System.out.print("------------------------------------\n");
                        System.out.printf("%-3s %-20.20s  %-20.20s%n", i, p.getNombre(), p.getEdad());
                        i++;
                    }
                    break;
                case 5:
                    // OPCIÖN 5: Modificar una persona
                    // Primero comprobamos si la lista no esta vacía;
                    if(personas.isEmpty()){
                        System.out.println("No se puede modificar ninguna persona. La lista está vacía.");
                        break;
                    }
                    
                    // Solicitar la posición a editar si la lista tiene elementos
                    posicionValida = false;
                    do{
                        try{
                            System.out.println("Indica la posición a modificar:");
                            posicionString = teclado.readLine();
                            posicion = Integer.parseInt(posicionString);
                            if(posicion < 0){
                                System.out.println("La posición debe ser un número positivo.");
                            }else if(posicion > (personas.size() - 1)){
                                System.out.println("La posición está fuera del rango, debe estar comprendida entre 0 y " + (personas.size() - 1));                               
                            }else{
                                posicionValida = true;
                            }
                        } catch (IOException e) {
                            System.out.println("Error al leer del teclado.");
                        } catch (NumberFormatException e){
                            System.out.println("La posición debe ser un número entero.");
                        }
                    } while(posicionValida == false);
                    
                    // Solicitar el nombre
                    nombreValido = false;
                    do{
                        try{
                            System.out.println("Teclea el nuevo nombre de la persona: ");
                            nombre = teclado.readLine();
                            if(nombre.length() == 0) {
                                System.out.println("El nombre debe estar relleno");
                            }else{
                                nombreValido = true;
                            }
                        } catch(IOException e){
                            System.out.println("Error al leer del teclado.");
                        }
                    } while(nombreValido == false);
                    
                    // Solictar la edad
                    edadValida = false;
                    do{
                        try{
                            System.out.println("Teclea la edad de " + nombre + ": ");
                            edadString = teclado.readLine();
                            edad = Integer.parseInt(edadString);
                            if(edad > 0){
                                edadValida = true;
                            }else{
                                System.out.println("La edad debe ser mayor que cero.");
                            }
                        } catch (IOException e) {
                            System.out.println("Error al leer del teclado.");
                        } catch (NumberFormatException e){
                            System.out.println("La edad debe ser un número entero.");
                        }
                    } while(edadValida == false);
                    
                    // Método 1: Actualizar el objeto de la lista eliminado el elemento a editar y añadiendo el nuevo,
                    // el nuevo objeto se añade al final de la lista ya que se va a usar una ordenación por nombre para mostralo
                    // personas.remove(posicion);
                    // personas.add(new Persona( nombre, edad ));
                    
                    // Método 2: Obtener una referencia al objeto original mediante un get y actualizar los datos mediante los métodos setter de la clase
                    // personas.get(posicion).setNombre(nombre);
                    // personas.get(posicion).setEdad(edad);
                    
                    // Método 3: Usando el método set
                    personas.set(posicion, new Persona( nombre, edad ));
                    
                    System.out.println("Persona creada correctamente.");
                    break;
                case 6:
                	// OPCIÓN 6: Limpiar la lista
                    personas.clear();
                    
                    System.out.println("Lista completamente borrada.");
                    break;
                case 7:
                	// OPCIÓN 7: Salir
                	
                    System.out.println("¡Hasta la próxima!");
                    salir = true;
                    break;
                default:
                    System.out.println("Selecciona una opción válida");
            }
        }
    }
    
    
}
