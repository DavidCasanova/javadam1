/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operacionesmatematicas;

import java.util.Scanner;
import java.util.regex.Matcher; 
import java.util.regex.Pattern; 

/**
 * @name OperacionesMatematicas
 * @description Calcular la suma, resta, multiplicación y división de dos número dados 
 * introducidos por teclado
 * @author David Casanova
 */
public class OperacionesMatematicas {
    
    // Capturar inputs en la consola
    private static Scanner scanner = new Scanner ( System.in );
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Obtener el valor para el primer número
    	System.out.print( "Teclee el valor del primer número: " );
    	String input1 = scanner.nextLine();
        
        // Obtener el valor para segundo número
    	System.out.print( "Teclee el valor del segundo número: " );
    	String input2 = scanner.nextLine();
        
        // Expresión regular para lo número con coma flotante
        String regex = "[+-]?[0-9]+(\\.[0-9]+)?([Ee][+-]?[0-9]+)?"; 
          
        // Compilar regex 
        Pattern pattern = Pattern.compile(regex); 
          
        // Comparar los valores de introducido con la expresión regular
        Matcher matcher_n1 = pattern.matcher(input1); 
        Matcher matcher_n2 = pattern.matcher(input2);
        
        // Si es un valor entero o decimal 
        if(matcher_n1.find() && matcher_n1.group().equals(input1) && matcher_n2.find() && matcher_n2.group().equals(input2)){
            // Convertir los valores introducidos en "float"
            float n1 = Float.parseFloat(input1);
            float n2 = Float.parseFloat(input2);
            
            // Sumar ambos números y mostrar el resultado
            float suma = n1 + n2;
            System.out.println("El resultado de la suma de " + n1 + " y " + n2 + " es: " + suma); 
            
            // Restar ambos números y mostrar el resultado
            float resta = n1 - n2;
            System.out.println("El resultado de la resta de " + n1 + " y " + n2 + " es: " + resta); 
            
            // Multiplicación ambos números y mostrar el resultado
            float multiplicacion = n1 * n2;
            System.out.println("El resultado de la multiplicación de " + n1 + " y " + n2 + " es: " + multiplicacion); 
            
            // Dividir ambos números y mostrar el resultado
            float division = n1 / n2;
            System.out.println("El resultado de la división de " + n1 + " y " + n2 + " es: " + division); 
        }else{
            // Si alguno de los valores introducidos no tiene el formato adecuado mostramos aviso
            System.out.println("Alguno de los valores introducidos no tiene el formato válido"); 
        }
    }
    
}
