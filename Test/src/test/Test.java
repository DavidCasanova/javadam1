/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import javax.swing.JOptionPane;


/**
 *
 * @author david
 */
public class Test {
    public static BufferedReader teclado = new BufferedReader( new InputStreamReader( System.in ) );

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean valor = false;
        
        do {
            try {
                String result = JOptionPane.showInputDialog(null, "Dime un valor");
                double d = Double.parseDouble(result);
                
                valor = true;
            } catch ( NumberFormatException e ){
                JOptionPane.showMessageDialog(null, "No has tecleado nada", "Error", JOptionPane.WARNING_MESSAGE);
            }
            
            
            
        } while( valor == false );
        
    }
    
}
