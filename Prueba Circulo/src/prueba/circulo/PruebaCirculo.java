/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prueba.circulo;

/**
 *
 * @author David Casanova Expósito
 */
class Circulo {
    
    // Atributos
    private double radio;
    private final double PI = 3.14;
    
    // Constructores
    public Circulo(double radio){
        this.radio = radio;
    }
    
    // Métodos
    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }
    
    public double area(){
        double area = PI * (radio * radio);
        return area;
    }
    
    public double perimetro(){
        double perimetro = 2 * PI * radio;
        return perimetro;
    }
    
}

public class PruebaCirculo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Circulo miCirculo = new Circulo(5);
        System.out.println(miCirculo.area());
        System.out.println(miCirculo.perimetro());
    }
    
}


