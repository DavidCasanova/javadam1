/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebacomplejos;

/**
 * @author David Casanova
 * @description 
 */
public class Complejo {
    
    // Atributos
    private double real, imag;
    
    // Contructores
    public Complejo(){
        
    }
    
    public Complejo(double real, double imag){
        this.real = real;
        this.imag = imag;
    }
    
    // Métodos
    public double consulta_Real(){
        return this.real;
    }
    
    public double consulta_Imag(){
        return this.imag;
    }
    
    public void cambia_Real(double real){
        this.real = real;
    }
    
    public void cambia_Imag(double imag){
        this.imag = imag;
    }
    
    public Complejo Sumar(Complejo numeroB){
        // Sumar reales
        double sumaReal = this.real + numeroB.consulta_Real();
        // Sumar imaginarios
        double sumaImag = this.imag + numeroB.consulta_Imag();
        
        // Nuevo complejo
        Complejo nuevoComplejo = new Complejo( sumaReal, sumaImag );
        
        return nuevoComplejo;
    }
    
    public boolean sonIguales( Complejo numeroB ){
        // Comparar las partes real e imaginaria de dos números complejos
        return ( Double.compare( this.real, numeroB.consulta_Real() ) == 0 ) && ( Double.compare( this.imag, numeroB.consulta_Imag()) == 0 );
    }
    
    public double calcularModulo(){
        return Math.sqrt( Math.pow( this.real, 2 ) + Math.pow( this.imag, 2 ) );
    }

}
