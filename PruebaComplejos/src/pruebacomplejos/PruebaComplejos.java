/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebacomplejos;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author David Casanova
 * @description
 */
public class PruebaComplejos {
    
    // Capturar inputs en la consola
    private static Scanner scanner = new Scanner ( System.in );

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      
        // Constructor con los valores por defecto
        System.out.println( "Instanciar al constructor de la clase \"Complejo \" sin parámetros." );
        Complejo sinParametros = new Complejo();
        System.out.println( "El valor real por defecto es: " + sinParametros.consulta_Real() );
        System.out.println( "El valor imaginario por defecto es: " + sinParametros.consulta_Imag() );
        
        // Constructor con valores por teclado
        System.out.println( "Instanciar al constructor de la clase \"Complejo \" con parámetros." );
        // Pedir el valor real
        System.out.print( "Teclea el valor de la parte real: " );
        String real = scanner.nextLine();
        // Pedir el valor imaginario
        System.out.print( "Teclea el valor de la parte imaginaria: " );
        String imag = scanner.nextLine();
        
        // Expresión regular para lo número con coma flotante
        String regex = "[+-]?[0-9]+(\\.[0-9]+)?([Ee][+-]?[0-9]+)?"; 
        // Compilar regex 
        Pattern pattern = Pattern.compile(regex); 
        
        // Comparar el valor introducido como real con la expresión regular
        Matcher matcher_real = pattern.matcher( real ); 
        // Comparar el valor introducido como imaginario con la expresión regular
        Matcher matcher_imag = pattern.matcher( imag ); 
        
        if(matcher_real.find() && matcher_real.group().equals(real) && matcher_imag.find() && matcher_imag.group().equals(imag)){
            double doubleReal = Double.parseDouble( real );
            double doubleImag = Double.parseDouble( imag );
            
            Complejo conParametros = new Complejo( doubleReal, doubleImag );
            System.out.println( "El valor real por defecto es: " + conParametros.consulta_Real() );
            System.out.println( "El valor imaginario por defecto es: " + conParametros.consulta_Imag() );
        } else {
            System.out.println( "El formato tecleado no es corecto." );
        }
        
        // Sumar dos número complejos
        System.out.println( "Sumar dos número complejos." );
        // Pedir el valor real del número A
        System.out.print( "Teclea el valor de la parte real del primer número: " );
        String realComplejoA = scanner.nextLine();
        // Pedir el valor imaginario del número A
        System.out.print( "Teclea el valor de la parte imaginaria del primer número: " );
        String imagComplejoA = scanner.nextLine();
        
        // Pedir el valor real del número B
        System.out.print( "Teclea el valor de la parte real del segundo número: " );
        String realComplejoB = scanner.nextLine();
        // Pedir el valor imaginario del número B
        System.out.print( "Teclea el valor de la parte imaginaria del segundo número: " );
        String imagComplejoB = scanner.nextLine();
        
        // Comparar el valor introducido como real del primer número con la expresión regular
        Matcher matcher_realA = pattern.matcher( realComplejoA ); 
        // Comparar el valor introducido como imaginario del primer número con la expresión regular
        Matcher matcher_imagA = pattern.matcher( imagComplejoA ); 
        
        // Comparar el valor introducido como real del segundo número con la expresión regular
        Matcher matcher_realB = pattern.matcher( realComplejoB ); 
        // Comparar el valor introducido como imaginario del segundo número con la expresión regular
        Matcher matcher_imagB = pattern.matcher( imagComplejoB ); 
        
        if(matcher_realA.find() && matcher_realA.group().equals(realComplejoA) && matcher_imagA.find() && matcher_imagA.group().equals(imagComplejoA)
           && matcher_realB.find() && matcher_realB.group().equals(realComplejoB) && matcher_imagB.find() && matcher_imagB.group().equals(imagComplejoB)){
            double doubleRealComplejoA = Double.parseDouble( realComplejoA );
            double doubleImagComplejoA = Double.parseDouble( imagComplejoA );
            
            double doubleRealComplejoB = Double.parseDouble( realComplejoB );
            double doubleImagComplejoB = Double.parseDouble( imagComplejoB );
            
            Complejo numeroA = new Complejo( doubleRealComplejoA, doubleImagComplejoA );
            Complejo numeroB = new Complejo( doubleRealComplejoB, doubleImagComplejoB );

            Complejo suma = numeroA.Sumar(numeroB);

            System.out.println( "El resultado de la suma es de " + suma.consulta_Real() + " en la parte real y " + suma.consulta_Imag() + " en la parte imaginaria." );
            
            // Comparar los dos número complejos
            boolean iguales = numeroA.sonIguales( numeroB );
            
            if( iguales == true ){
                System.out.println( "Los dos número complejos son iguales." );
            }else{
                System.out.println( "Los dos número complejos son diferentes." );
            }
            
            // Calcular al módulo del numeroA (a2 + b2)1/2
            System.out.println( "Calcular el Módulo de un número complejo." );

            double modulo = numeroA.calcularModulo();
            
            System.out.println( modulo );
        } else {
            System.out.println( "El formato tecleado no es corecto." );
        }
        
    }
    
}
