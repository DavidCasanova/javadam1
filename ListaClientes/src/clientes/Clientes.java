package clientes;

import java.io.Serializable;

/**
 * @author David Casanova
 */
public class Clientes implements Serializable {
    /**
     * Atributos
     */
    protected String nif, nombre, direccion;
    protected int telefono, edad;
    
    /**
     * Constructor
     */
    public Clientes( String nif, String nombre, int telefono, String direccion, int edad ) {
        this.nif = nif;
        this.nombre = nombre;
        this.telefono = telefono;
        this.direccion = direccion;
        this.edad = edad;
    }
    
    /**
     * Métodos
     */
    public void mostrarCliente(){
        System.out.println("\tNIF: " + this.nif);
        System.out.println("\tNombre: " + this.nombre);
        System.out.println("\tTeléfono: " + this.telefono);
        System.out.println("\tDirección: " + this.direccion);
        System.out.println("\tEdad: " + this.edad);
    }
    
}
