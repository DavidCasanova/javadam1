/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientes;

import java.io.*;
import java.text.SimpleDateFormat;

/**
 * @author David Casanova
 */
public class PruebaClientes {

    public static void main(String[] args) throws ClassNotFoundException, FileNotFoundException {
        // Capturar teclado
        BufferedReader teclado = new BufferedReader ( new InputStreamReader( System.in ));
        
        // Inicialización de variables
        String nif, nombre, direccion;
        int telefono, edad;
        Clientes cli;
        FileOutputStream archivo_guardar;
        FileInputStream archivo_leer;
        ObjectOutputStream clientes_guardar;
        ObjectInputStream clientes_leer;
        
        try {
            // Pedir el nombre el directorio donde alojar los datos
            System.out.println("\n¿En qué directorio quieres guardar el archivo?");
            String directorio = teclado.readLine();
            
            // Comprobar si existe el directorio
            if(!new File(directorio).exists()){
                // Crear el directorio
                boolean directorio_creado = (new File(directorio)).mkdir();
                if(directorio_creado){
                    System.out.println("Directorio: " + directorio + " creado!");
                } 
            }else{
                System.out.println("No se ha creado el directorio porque ya existía");
            }
            
            // Crear el archivo
            System.out.println("\n¿Qué nombre de archivo quieres usar?");
            String archivo = teclado.readLine();
            
            try {
                archivo_guardar = new FileOutputStream( directorio + archivo);
                System.out.println("Archivo: " + archivo + " creado!");

                // Solicitar el número de clientes a grabar
                System.out.println("\n¿Cuántos clientes quieres guardar en el archivo?");
                int cantidad = Integer.parseInt(teclado.readLine());

                // Grabar los clientes en el archivo
                clientes_guardar = new ObjectOutputStream(archivo_guardar);
                for(int i = 0; i < cantidad; i++){
                    System.out.println("\nIntroduce los datos para el cliente " + (i + 1) + ":");
                    
                    System.out.print("NIF del cliente: ");
                    nif = teclado.readLine();

                    System.out.print("Nombre del cliente: ");
                    nombre = teclado.readLine();

                    System.out.print("Teléfono del cliente: ");
                    telefono = Integer.parseInt(teclado.readLine());

                    System.out.print("Dirección del cliente: ");
                    direccion = teclado.readLine();

                    System.out.print("Edad del cliente: ");
                    edad = Integer.parseInt(teclado.readLine());

                    // Crear el objeto y almacenarlo
                    cli = new Clientes(nif, nombre, telefono, direccion, edad);
                    clientes_guardar.writeObject(cli);

                    System.out.println("\nCliente " + (i + 1) + " de un total de " + cantidad + " creado!");
                }

                // Mostrar todos los clientes que se han almacenado
                System.out.println("\n\nLos clientes almacenados en el archivo son:\n");
                archivo_leer = new FileInputStream(directorio + archivo);
                clientes_leer = new ObjectInputStream(archivo_leer);
                int contador = 1;
                while((cli = (Clientes) clientes_leer.readObject()) != null){
                    System.out.println("Cliente número " + contador + ":\n");
                    cli.mostrarCliente();
                    System.out.println("\n");
                    contador++;
                }
                clientes_leer.close();
                archivo_leer.close();
            } catch(FileNotFoundException e){
                System.out.println("\nError al crear el archivo: "+e.getMessage());
            }catch(EOFException e){
            	// Mostrar la fecha de la última modificación
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                System.out.println("\nEl archivo fue modificado por última vez el: " + sdf.format(new File(directorio + archivo).lastModified()));
            }catch(IOException e){
                System.out.println("\nError en el archivo: "+e.getMessage());
            }
        } catch ( IOException e ){
            System.out.println( "Error al leer del teclado." );
        }
        
    }
    
}
