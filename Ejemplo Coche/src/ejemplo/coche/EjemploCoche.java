package ejemplo.coche;

/**
 *
 * @author David Casanova Expósito
 */
public class EjemploCoche {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Coche coche1 = new Coche();
        Coche coche2 = new Coche("Renault", "Negro", 2000);
        
        coche1.setMarca("BMW");
        System.out.println(coche1.getMarca());
    }
    
}
