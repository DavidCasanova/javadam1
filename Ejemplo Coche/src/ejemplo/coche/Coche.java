package ejemplo.coche;

/**
 *
 * @author David Casanova  Expósito
 */
public class Coche {
    // Atributos
    private String marca;
    private String modelo;
    private String color;
    private double numKm;
    private byte numDePuertas;
    private short anioDeCompra;
    private boolean garantia;
    private String tipoDeCombustible;
    private boolean arrancado;
    private int aumentoVelocidad;
    private int velocidad;
    
    // Contructores
    public Coche(){
        
    }
    
    public Coche(String marca, String color, double numKm){
        this.marca = marca;
        this.color = color;
        this.numKm = numKm;
    }

    public Coche(String marca, String modelo, String color, double numKm, byte numDePuertas, short anioDeCompra, boolean garantia, String tipoDeCombustible) {
        this.marca = marca;
        this.modelo = modelo;
        this.color = color;
        this.numKm = numKm;
        this.numDePuertas = numDePuertas;
        this.anioDeCompra = anioDeCompra;
        this.garantia = garantia;
        this.tipoDeCombustible = tipoDeCombustible;
    }
    
    // Métodos
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getNumKm() {
        return numKm;
    }

    public void setNumKm(double numKm) {
        this.numKm = numKm;
    }

    public byte getNumDePuertas() {
        return numDePuertas;
    }

    public void setNumDePuertas(byte numDePuertas) {
        this.numDePuertas = numDePuertas;
    }

    public short getAnioDeCompra() {
        return anioDeCompra;
    }

    public void setAnioDeCompra(short anioDeCompra) {
        this.anioDeCompra = anioDeCompra;
    }

    public boolean isGarantia() {
        return garantia;
    }

    public void setGarantia(boolean garantia) {
        this.garantia = garantia;
    }

    public String getTipoDeCombustible() {
        return tipoDeCombustible;
    }

    public void setTipoDeCombustible(String tipoDeCombustible) {
        this.tipoDeCombustible = tipoDeCombustible;
    }

    public boolean isArrancado() {
        return arrancado;
    }

    public void setArrancado(boolean arrancado) {
        this.arrancado = arrancado;
    }

    public int getAumentoVelocidad() {
        return aumentoVelocidad;
    }

    public void setAumentoVelocidad(int aumentoVelocidad) {
        this.aumentoVelocidad = aumentoVelocidad;
    }

    public int getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(int velocidad) {
        this.velocidad = velocidad;
    }
    
    
    
    public void arrancar(){
        if( arrancado == false ){
            arrancado = true;
        }
    }
    
    public void acelerar(int aumentoVelocidad){
        velocidad += aumentoVelocidad;
    }
}
