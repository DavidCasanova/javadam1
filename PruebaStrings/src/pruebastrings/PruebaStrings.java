/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebastrings;

import java.util.Scanner;

/**
 *
 * @author david
 */
public class PruebaStrings {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Declaración de objetos
        Scanner teclado;
        String cadenaUno;
        String cadenaDos;
        
        // Crear el objeto Scanner
        teclado = new Scanner( System.in );
        
        // Crear el objeto String cadenaUno y asignarle un valor
        cadenaUno = "Texto de prueba";
        System.out.println( "Cadena uno declarada con el valor: Texto de prueba." );
        
        // Pedir al usurio el texto para la segunda cadena
        System.out.print( "Teclea el texto que desees: " );
        cadenaDos = teclado.nextLine();
        
        // Comparar las dos cadenas
        boolean iguales = cadenaUno.equals( cadenaDos );
        
        if( iguales == true ){
            System.out.println( "Las cadenas son iguales." );
        }else{
            System.out.println( "Las cadenas son diferentes." );
        }
        
        // Comparar el número de caracteres
        int longitudCadenaUno = cadenaUno.length();
        int longitudCadenaDos = cadenaDos.length();
        
        System.out.println( "La longitud de la cadena uno \"" + cadenaUno + "\" es: " + longitudCadenaUno );
        System.out.println( "La longitud de la cadena dos \"" + cadenaDos + "\" es: " + longitudCadenaDos );
        
        if( longitudCadenaUno == longitudCadenaDos ){
            System.out.println( "Las cadenas tienen la misma longitud." );
        }else{
            System.out.println( "Las cadenas son de diferente longitud." );
        }
        
        // Comprobar si la palabra "prueba" está contenida en la cadena dos
        boolean contiene = cadenaDos.contains( "prueba" );
        if( contiene == true ){
            System.out.println( "La cadena dos contiene la palabra \"prueba\"" );
        }else{
            System.out.println( "La cadena dos NO contiene la palabra \"prueba\"" );
        }
        
        // Pasar a minúsculas la cadena uno
        System.out.println( "Pasar a minúsculas la cadena uno: " + cadenaUno.toLowerCase() );
        
        // Pasar a mayúsculas la cadena 2
        System.out.println( "Pasar a mayúsculas la cadena dos: " + cadenaDos.toUpperCase() );
        
    }
    
}
