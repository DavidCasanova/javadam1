/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numerocercano;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/**
 *
 * @author David Casanova
 */
public class NumeroCercano {
    // Leer del teclado
    public static BufferedReader input = new BufferedReader ( new InputStreamReader ( System.in ));
            
    public static void main(String[] args) {
        int first_number, other_number, closest_number = 0, distance = 0;
        String line;
        boolean allIntegers = false;
        
        do {
            try {
                System.out.print( "Teclea el número de referencia: " );
                line = input.readLine();
                first_number = Integer.parseInt( line );
                
                for ( int i = 2; i <= 5; i++ ) {
                    System.out.print( "Teclea el número " + i + ": " );
                    line = input.readLine();
                    other_number = Integer.parseInt( line );
                    
                    if ( i == 2){
                        distance = Math.abs( first_number - other_number );
                        closest_number = other_number;
                    } else if ( Math.abs( first_number - other_number ) < distance ) {
                        distance = Math.abs( first_number - other_number );
                        closest_number = other_number;
                    }     
                }
                
                // Todos los número son enteros
                allIntegers = true;
                
                // Imprimir el resultado
                System.out.println( "El número más próximo a " + first_number + " es " + closest_number + " a una distancia de " + distance );
            } catch ( IOException e ) {
                System.out.println( "Error al leer del teclado." );
            } catch ( NumberFormatException e ) {
                System.out.println( "¡Cuidado!, el número debe ser entero." );
            }
        } while ( allIntegers == false );

    }
    
}
