/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resolver.ecuacion;

import java.util.Scanner;
import java.util.regex.Matcher; 
import java.util.regex.Pattern; 

/**
 * @name ResolverEcuacion
 * @description Resolver una ecuación de primer grado del tipo C1x + C1 = 0 donde los 
 * valores de C1 y C2 de introducen desde el teclado
 * @author David Casanova
 */
public class ResolverEcuacion {
    
    // Capturar inputs en la consola
    private static Scanner scanner = new Scanner ( System.in );
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Obtener el valor para C1
    	System.out.print( "Teclee el valor de C1: " );
    	String input1 = scanner.nextLine();
        
        // Obtener el valor para C2
    	System.out.print( "Teclee el valor de C2: " );
    	String input2 = scanner.nextLine();
        
        // Expresión regular para lo número con coma flotante
        String regex = "[+-]?[0-9]+(\\.[0-9]+)?([Ee][+-]?[0-9]+)?"; 
          
        // Compilar regex 
        Pattern pattern = Pattern.compile(regex); 
          
        // Comparar los valores de introducido con la expresión regular
        Matcher matcher_c1 = pattern.matcher(input1); 
        Matcher matcher_c2 = pattern.matcher(input2);
        
        // Si es un valor entero o decimal 
        if(matcher_c1.find() && matcher_c1.group().equals(input1) && matcher_c2.find() && matcher_c2.group().equals(input2)){
            // Convertir los valores introducidos en "float"
            float c1 = Float.parseFloat(input1);
            float c2 = Float.parseFloat(input2);
            
            // Resolver la ecuación C1x + C2 = 0 => x = (0 - C2) / C1
            float x = ( 0 - c2 ) / c1;
            
            // Mostrar el resultado por pantalla
            System.out.println("El valor de X es: " + x); 
        }else{
            // Si alguno de los valores introducidos no tiene el formato adecuado mostramos aviso
            System.out.println("Alguno de los valores introducidos no tiene el formato válido"); 
        }
    }

}
