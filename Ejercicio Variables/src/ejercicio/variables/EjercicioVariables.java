/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio.variables;

/**
 * @name EjercicioVariables
 * @description Tarea del tema 2 de la asignatura de 1º de Programación cuyo objetivo es,
 * según los supuestos dados, escoger el tipo de dato más adecuado para cada uno de ellos,
 * asignar el valor indicado y mostrar el resultado por consola.
 * @author David Casanova
 */
public class EjercicioVariables {
    
    /**
     * @description Métedo principal, desde aquí vamos a instanciar al reto de métodos para imprimir
     * el resultado en la consola
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        // Instanciar a la clase
        EjercicioVariables variable = new EjercicioVariables();
        
        // Instanciar al método esta_Casado e imprimir su resultado
        boolean casado = variable.esta_Casado();
        System.out.println( "El valor de la variable \"casado\" es " + casado );
        
        // Instanciar al método maximo e imprimir su resultado
        int MAXIMO = variable.maximo();
        System.out.println( "El valor de la variable \"MAXIMO\" es " + MAXIMO );
        
        // Instanciar al método dia_Semana e imprimir su resultado
        int diaSem = variable.dia_Semana();
        System.out.println( "El valor de la variable \"diaSem\" es " + diaSem );
        
        // Instanciar al método dia_Anual e imprimir su resultado
        int diaAnual = variable.dia_Anual();
        System.out.println( "El valor de la variable \"diaAnual\" es " + diaAnual );
        
        // Instanciar al método milisegundos e imprimir su resultado
        long miliseg = variable.milisegundos();
        System.out.println( "El valor de la variable \"miliseg\" es " + miliseg );
        
        // Instanciar al método obtener_Genero e imprimir su resultado
        Genero sexo = variable.obtener_Genero();
        System.out.println( "El valor de la variable \"sexo\" es " + sexo );
        
        // Instanciar al método total_Factura e imprimir su resultado
        float totalfactura = variable.total_Factura();
        System.out.println( "El valor de la variable \"totalfactura\" es " + totalfactura );
        
        // Instanciar al método total_Poblacion e imprimir su resultado
        long poblacion = variable.total_Poblacion();
        System.out.println( "El valor de la variable \"poblacion\" es " + poblacion );
    }
    
    /**
     * @name esta_Casado
     * @description Método que inicializa la variable "casado" como "booleano" y le
     * asigna el valor "true"
     * @return Valor asignado a la variable "casado"
     */
    public boolean esta_Casado(){
        /**
         * El tipo de dato adecuado es el "booleano" ya que el estado sólo puede tener
         * dos valores, "true" o "false" 
         */
        boolean casado = true;
        return casado;
    }
    
    /**
     * @name maximo
     * @description Método que inicializa "MAXIMO" como una constante (su valor no variará durante
     * todo el programa) y como "int", además se le asigna el valor "999999"
     * @return Valor asignado a la constante "MAXIMO"
     */
    public final int maximo(){
        /**
         * Al tratarse de un valor entero y que no será modificado se declara como tipo de dato contante 
         * "final" y entero "int"
         * El rango de valores del tipo de datos "int" es desde -2.147.483.648 hasta 2.147.483.647 por 
         * lo que 999999 está dentro de ese rango
         */
        final int MAXIMO = 999999;
        return MAXIMO;
    }
    
    /**
     * @name dia_Semana
     * @description Método que inicializa la variable "diaSem" como un dato tipo "byte"
     * @return Valor asignado a la variable "diaSem"
     */
    public byte dia_Semana(){
        /**
         * Los días de sólo pueden tener valores comprendidos entre el 1 y el 7
         * El tipo de dato "byte" acepta valores comprendidos entre -128 y 127 por lo que es el tipo de
         * dato adecuado para este caso
         */
        byte diaSem = 1;
        return diaSem;
    }
    
    /**
     * @name dia_Anual
     * @description Método que inicializa la variable "diaAnual" como un dato tipo "short"
     * @return Valor asignado a la variable "diaAnual"
     */
    public short dia_Anual(){
        /**
         * Los días del año puden tener valores comprendidos entre 1 y 365 (366 si es bisiesto)
         * A diferencia de los días de la semana, para los días del año no ppodemos usar el tipo de dato
         * "byte" porque queda fuera de rango
         * Por ello usamos el tipo de datos "short" cuyo rango de valores va desde -32.768 hasta 32.767
         */
        short diaAnual = 300;
        return diaAnual;
    }
    
    /**
     * @name milisegundos
     * @description Método que inicializa la variable "miliseg" como un dato tipo "long"
     * @return Valor asignado a la variable "miliseg"
     */
    public long milisegundos(){
        /**
         * Los valores en milisegundos (salvo aquellos que estén extremadamente próximos al 1 de enero
         * de 1970) están fuera del rango de valores admitodos en los tipos de datos "int"
         * Por ello debemos usar el tipo de dato "long" que tiene un rango de valores que va desde
         * -9.223.372.036.854.775.808 hasta 9.223.372.036.854.775.807
         */
        long miliseg = 1298332800000L;
        return miliseg;
    }
    
    /**
     * Lista de valores para el tipo de dato "enum"
     */
    public enum Genero {V, M};
    /**
     * @name obtener_Genero
     * @description Método que inicializa la variable "sexo" como un dato tipo "enum" denominado "Genero"
     * @return Valor asignado a la variable "sexo"
     */
    public Genero obtener_Genero(){
        /**
         * EL género, según el enunciado, tiene dos valores posibles, V (Varón) y M (Mujer)
         * Al tratarse de un conjunto de valores restringidos la mejor manera de porceder es mediante
         * el uso de una lista de valores enumerada "enum"
         */
        Genero sexo = Genero.M;
        return sexo;
    }
    
    /**
     * @name total_Factura
     * @description Método que inicializa la variable "total" como un dato tipo "float"
     * @return Valor asignado a la variable "totalFactura"
     */
    public float total_Factura(){
        /**
         * Los importes de facturas pueden contener decimales, por ello usamos el tipo de dato "float"
         * Tiene un rango de valores válidos muy grande pero inferior al tipo de dato "double" por lo que
         * se optimiza el consumo de recursos
         */
        float totalFactura = 10350.678F;
        return totalFactura;
    }
    
    /**
     * @name total_Poblacion
     * @description Método que inicializa la variable "poblacion" como un dato tipo "long"
     * @return Valor asignado a la variable "poblacion"
     */
    public long total_Poblacion(){
        /**
         * El número total de habitantes mundiales excede el rango permitido para el tipo de dato "int"
         * Por ello debemos recurrir al tipo de dato "long"
         */
        long poblacion = 6775235741L;
        return poblacion;
    }
    
}
