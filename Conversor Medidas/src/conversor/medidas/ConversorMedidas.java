/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversor.medidas;

import java.util.Scanner;
import java.util.regex.Matcher; 
import java.util.regex.Pattern; 

/**
 * @name ConversorMedidas
 * @description Introducir una medida en metros, convertirla en centímetros y mostrar el resultado por pantalla
 * Introducir una medida en centímetros, convertirlo en metros y mostrar el resultado por pantalla
 * @author david
 */
public class ConversorMedidas {
    
    // Capturar inputs en la consola
    private static Scanner scanner = new Scanner ( System.in );

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Expresión regular para lo número con coma flotante
        String regex = "[+-]?[0-9]+(\\.[0-9]+)?([Ee][+-]?[0-9]+)?"; 
          
        // Compilar regex 
        Pattern pattern = Pattern.compile(regex); 
        
        // Obtener una mmedida en centímetros 
    	System.out.print( "Teclee una medida en centímetros: " );
    	String input1 = scanner.nextLine();
        
        // Comparar el valor introducido con la expresión regular
        Matcher matcher_cm = pattern.matcher(input1); 
        
        // Si es un valor entero o decimal 
        if(matcher_cm.find() && matcher_cm.group().equals(input1)){
            // Convertir los valores introducidos en "float"
            float centimetros = Float.parseFloat(input1);
            
            // Convertir la cantidad introducida en metros
            float metros = centimetros / 100;
            
            // Mostrar el resultado por pantalla
            System.out.println("El valor en metros de " + centimetros + " centímetros es: " + metros); 
        }else{
            // Si el valor introducido no tiene el formato adecuado mostramos aviso
            System.out.println("El valor introducido no tiene el formato válido"); 
        }
        
        // Obtener una mmedida en metros 
    	System.out.print( "Teclee una medida en metros: " );
    	String input2 = scanner.nextLine();
        
        // Comparar el valor introducido con la expresión regular
        Matcher matcher_m = pattern.matcher(input2); 
        
        // Si es un valor entero o decimal 
        if(matcher_m.find() && matcher_m.group().equals(input2)){
            // Convertir los valores introducidos en "float"
            float metros = Float.parseFloat(input2);
            
            // Convertir la cantidad introducida en metros
            float centimetros = metros * 100;
            
            // Mostrar el resultado por pantalla
            System.out.println("El valor en centímetros de " + metros + " metros es: " + centimetros); 
        }else{
            // Si el valor introducido no tiene el formato adecuado mostramos aviso
            System.out.println("El valor introducido no tiene el formato válido"); 
        }
    }
    
}
