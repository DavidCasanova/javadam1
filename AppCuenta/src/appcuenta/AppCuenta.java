/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appcuenta;

/**
 * @descrprition Tarea 1 del tema 3. Crear dos cuentas mediante el constructor de la clase Cuenta.
 * Hacer un ingreso en cada cuenta.
 * Hacer una retirada en cada cuenta.
 * Mostrar el saldo final en cada una de ellas.
 * @author David Casanova Expósito
 */
public class AppCuenta {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Crear las cuentas
        System.out.println("Crear 2 cuentas");
        
        Cuenta cuenta1 = new Cuenta("Titular 1", 100);
        Cuenta cuenta2 = new Cuenta("Titular 2", 200);
        
        // Mostrar los titulares y el saldo inicial
        System.out.println("Cuenta 1 creada para el titular " + cuenta1.getTitular() + " con el saldo inicical de " + cuenta1.getSaldo());
        System.out.println("Cuenta 2 creada para el titular " + cuenta2.getTitular() + " con el saldo inicical de " + cuenta2.getSaldo());
    
        // Ingresos
        cuenta1.ingresar( 150.50 );
        cuenta2.ingresar( 200.75 );
        
        System.out.println("Se ingresa en la cuenta de " + cuenta1.getTitular() + " la cantidad de 100.50");
        System.out.println("Se ingresa en la cuenta de " + cuenta2.getTitular() + " la cantidad de 200.75");
        
        // Retiradas
        cuenta1.retirar( 85 );
        cuenta2.retirar( 10 );
        
        System.out.println("Se retira de la cuenta de " + cuenta1.getTitular() + " la cantidad de 85");
        System.out.println("Se retira de la cuenta de " + cuenta2.getTitular() + " la cantidad de 10");
        
        // Mostrar el saldo actual
        System.out.println("El saldo actual de la cuenta de " + cuenta1.getTitular() + " es " + cuenta1.getSaldo());
        System.out.println("El saldo actual de la cuenta de " + cuenta2.getTitular() + " es " + cuenta2.getSaldo());
        
    }
    
}
