/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appcuenta;

/**
 * @description Contiene los constructores que permiten crear una cuenta con el titular o con titular y saldo inicial.
 * Además, a parte de los Setters y Getter, sipone los los métodos para realizar un ingreso y una retirada en la cuenta dada.
 * @author David Casanova Expósito
 */
public class Cuenta {
    
    // Atributos
    private String titular;
    private double saldo;
    
    // Contructores
    public Cuenta( String titular ) {
        this.titular = titular;
    }
    
    public Cuenta( String titular, double saldo ) {
        this.titular = titular;
        this.saldo = saldo;
    }
    
    // Métodos
    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
    
    public void ingresar( double cantidad ) {
        saldo += cantidad;
    }
    
    public void retirar( double cantidad ) {
        saldo -= cantidad;
    }
    
}
