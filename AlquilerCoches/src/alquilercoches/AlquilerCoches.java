package alquilercoches;

/**
 * @author David Casanova
 */
public class AlquilerCoches {
    // Atributos
    private String matricula, marca, modelo;
    private char tipo;
    private double precio;
    private int num_dias, dd, mm, aa;
    private boolean alquilado;
    private final int MIN_DIAS = 2;
    
    // Constructor sin parámetros
    public AlquilerCoches() {
        
    }
    
    // Constructor con parámetros
    public AlquilerCoches(String matricula, String marca, String modelo, char tipo, double precio){
        // Setear las variables
        this.matricula = matricula;
        this.marca = marca;
        this.modelo = modelo;
        this.tipo = tipo;
        this.precio =  precio;
    }
  
    // Setters
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
    public void setMarca(String marca) {
        this.marca = marca;
    }
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    public void setTipo(char tipo) {
        this.tipo = tipo;
    }
    public void setPrecio(double precio) {
        this.precio = precio;
    }
    public void setNum_dias(int num_dias) {
        this.num_dias = num_dias;
    }
    public void setDd(int dd) {
        this.dd = dd;
    }
    public void setMm(int mm) {
        this.mm = mm;
    }
    public void setAa(int aa) {
        this.aa = aa;
    }
    public void setAlquilado(boolean alquilado) {
        this.alquilado = alquilado;
    }
    
    // Getters
    public String getMatricula() {
        return matricula;
    }
    public String getMarca() {
        return marca;
    }
    public String getModelo() {
        return modelo;
    }
    public char getTipo() {
        return tipo;
    }
    public double getPrecio() {
        return precio;
    }
    public int getNum_dias() {
        return num_dias;
    }
    public int getDd() {
        return dd;
    }
    public int getMm() {
        return mm;
    }
    public int getAa() {
        return aa;
    }
    public boolean isAlquilado() {
        return alquilado;
    }
    
    // Métodos
    
    
}
