/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package supuestouno;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/**
 * @author David Casanova
 */
public class SupuestoUno {
    // Capturar input del teclado
    public static BufferedReader teclado = new BufferedReader ( new InputStreamReader( System.in ));
   
    public static void main(String[] args) {
        // Declarar variables
        String excluirStr = "";
        int excluirInt = 0;
        boolean esInt = false, rangoValido = false;
        
        do {
            try {
                System.out.print( "Teclea el número de la tabla a excluir (debe estar entre el 2 y el 10): " );
                excluirStr = teclado.readLine();
                excluirInt = Integer.parseInt( excluirStr );
                
                // Si no hay excepciones entonces el número tecleaado
                esInt = true;
                
                /* 
                * Como sólo vamos a imprimir las tablas de multiplicar entre el 2 y el 10 debemos controlar que
                * el número tecleado esté entre el 2 y el 10
                */
                if( excluirInt >= 2 && excluirInt <=10 ){
                    rangoValido = true;
                } else {
                    System.out.println( "El número debe estar entre 2 y 10." );
                }
            } catch ( IOException e ) {
                System.out.println( "Error al leer del teclado." );
            } catch ( NumberFormatException e) {
                System.out.println( "Debes teclear un número entero." );
            }
        } while( esInt == false || rangoValido == false );
        
        // Generar la tabla de multiplicar
        for( int i = 2; i <= 10; i++ ){
            System.out.println( "Tabla del " + i + ":" );
            if( i != excluirInt ){
                for( int j = 1; j <= 10; j++ ){
                    System.out.println( i + " x " + j + " = " + ( i * j ) );
                }
                System.out.println( " " );
            }
        }
    }
    
}
